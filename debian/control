Source: pelican
Section: web
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Ondřej Surý <ondrej@debian.org>,
 Vincent Cheng <vcheng@debian.org>,
 Geert Stappers <stappers@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 furo,
 pybuild-plugin-pyproject,
 python3-all,
 python3-blinker,
 python3-dateutil,
 python3-docutils,
 python3-feedgenerator,
 python3-jinja2,
 python3-pdm-backend,
 python3-pygments,
 python3-rich,
 python3-sphinx,
 python3-sphinxext-opengraph,
 python3-tz,
 python3-unidecode,
 python3-watchfiles,
Standards-Version: 4.7.0
Homepage: https://getpelican.com/
Vcs-Git: https://salsa.debian.org/python-team/packages/pelican.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pelican
Rules-Requires-Root: no

Package: pelican
Architecture: all
Depends:
 python3,
 python3-markdown,
 ${misc:Depends},
 ${python3:Depends}
Suggests:
 pandoc,
 pelican-doc,
 python3-bs4
Description: blog aware, static website generator
 Pelican is a static site generator, written in Python.  It allows you
 to write your weblog entries directly with your editor of choice in
 reStructuredText or Markdown, and generates completely static output
 that is easy to host anywhere.  Pelican includes a simple CLI tool to
 (re)generate the weblog and it is easy to interface with DVCSes and web
 hooks.

Package: pelican-doc
Architecture: all
Section: doc
Built-Using:
 ${sphinxdoc:Built-Using}
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Suggests:
 pelican
Multi-Arch: foreign
Description: blog aware, static website generator (documentation)
 Pelican is a static site generator, written in Python.  It allows you
 to write your weblog entries directly with your editor of choice in
 reStructuredText or Markdown, and generates completely static output
 that is easy to host anywhere.  Pelican includes a simple CLI tool to
 (re)generate the weblog and it is easy to interface with DVCSes and web
 hooks.
 .
 This package provides documentation for Pelican.
